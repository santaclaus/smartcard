# Applications for smart card reader #

This application is cross-platform.
IDE development Qt 4.8.6.

The application works with smart card reader ACS.
The tests were run on smart cards MIFARE ID.

Supported platforms:
* Windovs (winscard)
* Linux (pcsclite)

## How to fast start ##

### Windows: ###
* Download source code
* Install the drivers on the smart card reader windows
* Compile on Qt 4.8
* The first time you select a smart card reader
* Change can be read via the context menu (click the right mouse button)

### Linux: ###
Setting up the environment for example Ubuntu 14 x32.
To support **pcsclite** the need to setup:

```
#!bash

sudo apt-get install  libglew-dev libcheese7 libcheese-gtk23 libclutter-gst-2.0-0 libcogl15 libclutter-gtk-1.0-0 libclutter-1.0-0
sudo apt-get install libqt4-dev
sudo apt-get install pcscd
sudo apt-get install libpcsclite-dev
```
Then follow the same standard actions:
* Download source code
* Install the drivers on the smart card reader for linux
* Compile on Qt 4.8
* The first time you select a smart card reader
* Change can be read via the context menu (click the right mouse button)

## For questions contact ##

* danilg66@gmail.com