QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SmartCard
TEMPLATE = app

VERSION = 1.0.0

win32 {
LIBS += -lwinscard
}
unix {
LIBS += -lpcsclite
INCLUDEPATH += -I "/usr/include/PCSC"
}

SOURCES += \
    main.cpp \
    pcsc.cpp \
    settingsdialog.cpp \
    smartcardreader.cpp

FORMS += \
    settingsdialog.ui \
    smartcardreader.ui

HEADERS += \
    pcsc.h \
    settingsdialog.h \
    smartcardreader.h
