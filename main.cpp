#include <QApplication>
#include <QSettings>

#include "smartcardreader.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSettings::setPath(QSettings::IniFormat, QSettings::SystemScope, ".");
    SmartCardReader smart_card_reader;
    smart_card_reader.show();

    return a.exec();
}
