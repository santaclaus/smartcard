#include "pcsc.h"

#include <QTimer>
#include <QSettings>
#include <QDebug>

#include <winscard.h>

const BYTE baAtrMifare1K[] = {0x3B,						// Initial
                              0x8F,						// T0
                              0x80,						// TD1
                              0x01,						// TD2
                              0x80,						// Category Indicator
                              0x4F,						// Appl. Id. Precence Indicator
                              0x0C,						// Tag Length
                              0xA0,0x00,0x00,0x03,0x06,	// AID
                              0x03,						// SS
                              0x00,0x01,				// NN - Mifare 1k
                              0x00,0x00,0x00,0x00,		// r.f.u
                              0x6A};					// TCK

const BYTE baAtrMifare4K[] = {0x3B,						// Initial
                              0x8F,						// T0
                              0x80,						// TD1
                              0x01,						// TD2
                              0x80,						// Category Indicator
                              0x4F,						// Appl. Id. Precence Indicator
                              0x0C,						// Tag Length
                              0xA0,0x00,0x00,0x03,0x06,	// AID
                              0x03,						// SS
                              0x00,0x02,				// NN - Mifare 4k
                              0x00,0x00,0x00,0x00,		// r.f.u
                              0x69};					// TCK

const BYTE baAtrMifareUL[] = {0x3B,						// Initial
                              0x8F,						// T0
                              0x80,						// TD1
                              0x01,						// TD2
                              0x80,						// Category Indicator
                              0x4F,						// Appl. Id. Precence Indicator
                              0x0C,						// Tag Length
                              0xA0,0x00,0x00,0x03,0x06,	// AID
                              0x03,						// SS
                              0x00,0x03,				// NN - Mifare Ultra Light
                              0x00,0x00,0x00,0x00,		// r.f.u
                              0x68};					// TCK

const BYTE baAtrMifareID[] = {0x3B,						// Initial
                              0x8F,						// T0
                              0x80,						// TD1
                              0x01,						// TD2
                              0x80,						// Category Indicator
                              0x4F,						// Appl. Id. Precence Indicator
                              0x0C,						// Tag Length
                              0xA0,0x00,0x00,0x03,0x06,	// AID
                              0x03,						// SS
                              0x00,0x26,				// NN - Mifare ID
                              0x00,0x00,0x00,0x00,		// r.f.u
                              0x4D};					// TCK

#define IOCTL_SMARTCARD_VENDOR_IFD_EXCHANGE     SCARD_CTL_CODE(1)
#define IOCTL_SMARTCARD_VENDOR_VERIFY_PIN       SCARD_CTL_CODE(2)
#define IOCTL_SMARTCARD_VENDOR_MODIFY_PIN       SCARD_CTL_CODE(3)
#define IOCTL_SMARTCARD_VENDOR_TRANSFER_PIN     SCARD_CTL_CODE(4)

PCSC::PCSC(QObject *parent) : QThread(parent)
{
    moveToThread(this);
}

PCSC::~PCSC()
{

}

void PCSC::run()
{
    //connect();
    //readCard();
    exec();
}

void PCSC_STATUS(LONG lRetValue, char* msg)
{
}

void PCSC_ERROR(LONG lRetValue, char* msg)
{
}

void PCSC_EXIT_ON_ERROR(LONG lRetValue)
{
}

QString PCSC::getScardErrorMessage(int code)
{
    switch(code)
    {
    // Smartcard Reader interface errors
    case SCARD_E_CANCELLED:
        return ("The action was canceled by an SCardCancel request.");
        break;
    case SCARD_E_CANT_DISPOSE:
        return ("The system could not dispose of the media in the requested manner.");
        break;
    case SCARD_E_CARD_UNSUPPORTED:
        return ("The smart card does not meet minimal requirements for support.");
        break;
    case SCARD_E_DUPLICATE_READER:
        return ("The reader driver didn't produce a unique reader name.");
        break;
    case SCARD_E_INSUFFICIENT_BUFFER:
        return ("The data buffer for returned data is too small for the returned data.");
        break;
    case SCARD_E_INVALID_ATR:
        return ("An ATR string obtained from the registry is not a valid ATR string.");
        break;
    case SCARD_E_INVALID_HANDLE:
        return ("The supplied handle was invalid.");
        break;
    case SCARD_E_INVALID_PARAMETER:
        return ("One or more of the supplied parameters could not be properly interpreted.");
        break;
    case SCARD_E_INVALID_TARGET:
        return ("Registry startup information is missing or invalid.");
        break;
    case SCARD_E_INVALID_VALUE:
        return ("One or more of the supplied parameter values could not be properly interpreted.");
        break;
    case SCARD_E_NOT_READY:
        return ("The reader or card is not ready to accept commands.");
        break;
    case SCARD_E_NOT_TRANSACTED:
        return ("An attempt was made to end a non-existent transaction.");
        break;
    case SCARD_E_NO_MEMORY:
        return ("Not enough memory available to complete this command.");
        break;
    case SCARD_E_NO_SERVICE:
        return ("The smart card resource manager is not running.");
        break;
    case SCARD_E_NO_SMARTCARD:
        return ("The operation requires a smart card, but no smart card is currently in the device.");
        break;
    case SCARD_E_PCI_TOO_SMALL:
        return ("The PCI receive buffer was too small.");
        break;
    case SCARD_E_PROTO_MISMATCH:
        return ("The requested protocols are incompatible with the protocol currently in use with the card.");
        break;
    case SCARD_E_READER_UNAVAILABLE:
        return ("The specified reader is not currently available for use.");
        break;
    case SCARD_E_READER_UNSUPPORTED:
        return ("The reader driver does not meet minimal requirements for support.");
        break;
    case SCARD_E_SERVICE_STOPPED:
        return ("The smart card resource manager has shut down.");
        break;
    case SCARD_E_SHARING_VIOLATION:
        return ("The smart card cannot be accessed because of other outstanding connections.");
        break;
    case SCARD_E_SYSTEM_CANCELLED:
        return ("The action was canceled by the system, presumably to log off or shut down.");
        break;
    case SCARD_E_TIMEOUT:
        return ("The user-specified timeout value has expired.");
        break;
    case SCARD_E_UNKNOWN_CARD:
        return ("The specified smart card name is not recognized.");
        break;
    case SCARD_E_UNKNOWN_READER:
        return ("The specified reader name is not recognized.");
        break;
    case SCARD_F_COMM_ERROR:
        return ("An internal communications error has been detected.");
        break;
    case SCARD_F_INTERNAL_ERROR:
        return ("An internal consistency check failed.");
        break;
    case SCARD_F_UNKNOWN_ERROR:
        return ("An internal error has been detected, but the source is unknown.");
        break;
    case SCARD_F_WAITED_TOO_LONG:
        return ("An internal consistency timer has expired.");
        break;
    case SCARD_W_REMOVED_CARD:
        return ("The smart card has been removed and no further communication is possible.");
        break;
    case SCARD_W_RESET_CARD:
        return ("The smart card has been reset, so any shared state information is invalid.");
        break;
    case SCARD_W_UNPOWERED_CARD:
        return ("Power has been removed from the smart card and no further communication is possible.");
        break;
    case SCARD_W_UNRESPONSIVE_CARD:
        return ("The smart card is not responding to a reset.");
        break;
    case SCARD_W_UNSUPPORTED_CARD:
        return ("The reader cannot communicate with the card due to ATR string configuration conflicts.");
        break;
    case ERROR_INVALID_FUNCTION:
        return ("The system cannot find the file specified.");
        break;
    }
    return ("Error is not documented.");
}

LONG PCSC::connect()
{
    LONG lRetValue;
    lRetValue = SCardEstablishContext(SCARD_SCOPE_SYSTEM,//SCARD_SCOPE_USER,
                                      NULL,
                                      NULL,
                                      &m_handle_context);
    if (lRetValue == SCARD_S_SUCCESS) {
        qDebug() << "Resource manager OK";
    }
    PCSC_STATUS(lRetValue,"SCardEstablishContext");
    QSettings settings("settings.ini", QSettings::IniFormat);
    m_reader = settings.value("reader_name").toString();
    strcpy(m_size_selected_reader, m_reader.toStdString().c_str());

    return lRetValue;
}

LONG PCSC::waitForCardPresent()
{
    m_card_pressent = false;
    SCARD_READERSTATE sReaderState;
    LONG lRetValue;

#ifdef Q_OS_WIN
    wchar_t* wString=new wchar_t[4096];
    MultiByteToWideChar(CP_ACP,
                        0,
                        m_reader.toStdString().c_str(),
                        -1,
                        wString,
                        4096);
    sReaderState.szReader = wString;
#endif

#ifdef Q_OS_LINUX
    sReaderState.szReader = m_reader.toStdString().c_str();
#endif
    sReaderState.dwCurrentState = SCARD_STATE_UNAWARE;
    sReaderState.dwEventState = SCARD_STATE_UNAWARE;
    lRetValue = SCardGetStatusChange(m_handle_context,
                    CARD_WAIT_TIME_MSEC,
                    &sReaderState,
                    1);
    PCSC_STATUS(lRetValue,"SCardGetStatusChange");
    if((sReaderState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT) {
        m_card_pressent = true;
    }

    return lRetValue;
}

LONG PCSC::activateCard()
{
    LONG lRetValue;

#ifdef Q_OS_WIN
    wchar_t* wString=new wchar_t[4096];
    MultiByteToWideChar(CP_ACP,
                        0,
                        m_reader.toStdString().c_str(),
                        -1,
                        wString,
                        4096);
    lRetValue = SCardConnect(
                    m_handle_context,
                    wString,
                    SCARD_SHARE_EXCLUSIVE,
                    SCARD_PROTOCOL_T1,
                    &m_handle_card,
                    &m_active_protocol);
#endif

#ifdef Q_OS_LINUX
    lRetValue = SCardConnect(
                    m_handle_context,
                    m_reader.toStdString().c_str(),
                    SCARD_SHARE_EXCLUSIVE,
                    SCARD_PROTOCOL_T1,
                    &m_handle_card,
                    &m_active_protocol);
#endif

    PCSC_STATUS(lRetValue, "SCardConnect");
    switch(m_active_protocol)
    {
        case SCARD_PROTOCOL_T0:
            // Smart card protocol T=0
            break;
        case SCARD_PROTOCOL_T1:
            // Smart card protocol T=1
            break;
        case SCARD_PROTOCOL_UNDEFINED:
            // Smart card protocol undefined
            lRetValue = -1;
            break;
    }
    return lRetValue;
}

LONG PCSC::disconnect()
{
    long lRetValue;

    lRetValue  = SCardDisconnect(m_handle_card, SCARD_UNPOWER_CARD);
    PCSC_STATUS(lRetValue, "SCardDisconnect");
    lRetValue =	SCardReleaseContext(m_handle_context);
    m_handle_context = 0;
    return lRetValue;
}

QStringList PCSC::getReaders()
{
    LONG lRetValue;
    LPTSTR pmszReaders = NULL;
    LPTSTR pszReader;
    DWORD cch = SCARD_AUTOALLOCATE;
    QStringList readers;

    lRetValue = SCardEstablishContext(SCARD_SCOPE_USER,
                                      NULL,
                                      NULL,
                                      &m_handle_context);

    lRetValue = SCardListReaders(
                    m_handle_context,
                    NULL,
                    (LPTSTR)&pmszReaders,
                    &cch );
    PCSC_STATUS(lRetValue,"SCardListReaders");
    pszReader = pmszReaders;
    if (pszReader == NULL)
        return QStringList();
    while ( *pszReader != '\0' ) {
#ifdef Q_OS_WIN
        readers.append(QString::fromWCharArray(pszReader, wcslen(pszReader)));
        pszReader = pszReader + wcslen((wchar_t *)pszReader) + 1;
#endif

#ifdef Q_OS_LINUX
        readers.append(QString::fromLocal8Bit(pszReader, strlen(pszReader)));
        pszReader = pszReader + strlen(pszReader) + 1;
#endif

    }
    lRetValue = SCardFreeMemory( m_handle_context, pmszReaders );
    PCSC_ERROR(lRetValue, "SCardFreeMemory");

    return readers;
}

#define MAX_BUFFER_SIZE 300;
#define MAX_ATR_SIZE 300;
#define MAX_READERNAME 300;

void PCSC::emulationCard()
{
    LONG lReturn;
    SCARDCONTEXT hContext;
    DWORD dwReaders;
    LPTSTR mszReaders;
    char *ptr, **readers = NULL;
    int nbReaders;
    SCARDHANDLE hCard;
    DWORD dwActiveProtocol, dwReaderLen, dwState, dwProt, dwAtrLen;
    BYTE pbAtr[300];
    char pbReader[300];
    int reader_nb;
    int i, offset;

    unsigned char bRecvBuffer[300];
    DWORD length;
    unsigned char attribute[1];
    DWORD attribute_length;
    SCARD_IO_REQUEST pioRecvPci;

    // ������������� API
    connect();

    // ���������� ����� ����������� ��� ����������
    wchar_t* wString=new wchar_t[4096];
    MultiByteToWideChar(CP_ACP,
                        0,
                        m_reader.toStdString().c_str(),
                        -1,
                        wString,
                        4096);

    m_handle_card = 0;
    m_active_protocol = 0;

    // ����������� � ����������� � ������ "��� �����"
    int rv = SCardConnect(m_handle_context,
                     wString,
                     SCARD_SHARE_DIRECT,
                     0,
                     &m_handle_card,
                     &m_active_protocol);

    if (rv == SCARD_S_SUCCESS) {
        qDebug() << "Connect OK" << lReturn;
    } else {
        qDebug() << "Connect NOT";
    }


    // � ��� � ����� ���������� ������������
    BYTE bSendBuffer[10] = { 0xE0, 0x00, 0x00, 0x18, 0x00 };

    WORD bytesReturned = 0;
    //bRecvBuffer[length] = '\0';
    length = 0;
    DWORD controlcode = SCARD_CTL_CODE(3500);
    qDebug() << "control code" << controlcode;
    rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 5,
                           bRecvBuffer, sizeof(bRecvBuffer), &length);

    if (rv == SCARD_S_SUCCESS) {
        qDebug() << "Control OK" << rv << length;
        qDebug() << " Firmware: ";
        QString st;
        QByteArray bb;
        for (i=0; i < length; i++) {
            st = st + bRecvBuffer[i];

            qDebug() << bRecvBuffer[i];
        }

        QString hextodec(toHexString(bRecvBuffer, length));
        qDebug() << hextodec;
        parseAPDUResponse(hextodec);
        bRecvBuffer[length] = '\0';
    } else {
        qDebug() << "Control NOT" << rv << getScardErrorMessage(rv);
    }

    // ��������� ��������
    memcpy(bSendBuffer, "\xE0\x00\x00\x59\x02\x00\xC8", 7);
    length = sizeof(bRecvBuffer);
    rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 7,
                      bRecvBuffer, sizeof(bRecvBuffer), &length);
    if (rv == SCARD_S_SUCCESS) {
        QString hextodec(toHexString(bRecvBuffer, length));
        qDebug() << "respon timeout command" << hextodec;
        parseAPDUResponse(hextodec);
    } else {
        qDebug() << "fail timeout";
    }

    // ���� � ����� Target Mode-related Command (5.9.2 ACR API Document)
    memcpy(bSendBuffer, "\xE0\x00\x00\x51\x02\x02\x02", 7);
    length = sizeof(bRecvBuffer);
    rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 7,
                      bRecvBuffer, sizeof(bRecvBuffer), &length);
    if (rv == SCARD_S_SUCCESS) {
        QString hextodec(toHexString(bRecvBuffer, length));
        qDebug() << "respon target-mode command" << hextodec;
        parseAPDUResponse(hextodec);
    } else {
        qDebug() << "fail target mode";
    }
    waitRequestData();

//    //int p = 1200;
//    m_respon_apdu_code = "6300";
//    while(m_respon_apdu_code == "6300") {
//        // �������� ������� ������
//        memcpy(bSendBuffer, "\xE0\x00\x00\x58\x00", 5);
//        length = sizeof(bRecvBuffer);
//        rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 5,
//                          bRecvBuffer, sizeof(bRecvBuffer), &length);
//        if (rv == SCARD_S_SUCCESS) {
//            QString hextodec(toHexString(bRecvBuffer, length));
//          //  qDebug() << "recive data command" << hextodec;
//            parseAPDUResponse(hextodec);
//        } else {
//        //    qDebug() << "fail recive data";
//        }
////        qDebug() << "string" << m_respon_apdu_code;
//      //  p--;
//    }
//    qDebug() << "EXIT" << m_respon_apdu_code;

    // SEND ATR command
//    memcpy(bSendBuffer, "\xE0\x00\x00\x42\xLEN\x01\x02\x", 8);
//    length = sizeof(bRecvBuffer);
//    rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 8,
//                      bRecvBuffer, sizeof(bRecvBuffer), &length);
//    if (rv == SCARD_S_SUCCESS) {
//        QString hextodec(toHexString(bRecvBuffer, length));
//        qDebug() << "second command" << hextodec;
//        parseAPDUResponse(hextodec);
//    } else {
//        qDebug() << "fail";
    //    }
}

void PCSC::enterCommandState()
{

}

void PCSC::stopEmulationCard()
{
    int rv = SCardDisconnect(m_handle_card, SCARD_UNPOWER_CARD);
    if (rv == SCARD_S_SUCCESS) {
        qDebug() << "Disconnect from card OK";
        m_handle_card = 0;
    } else {
        qDebug() << "FAIL disconnect from card";
    }


    PCSC_STATUS(rv, "SCardDisconnect");


    rv = SCardReleaseContext(m_handle_context);
    if (rv == SCARD_S_SUCCESS) {
        qDebug() << "Release reader context OK";
        m_handle_context = 0;
    } else {
        qDebug() << "FAIL release reader context";
    }
}

void PCSC::waitRequestData()
{
    LONG lReturn;
    SCARDCONTEXT hContext;
    DWORD dwReaders;
    LPTSTR mszReaders;
    char *ptr, **readers = NULL;
    int nbReaders;
    SCARDHANDLE hCard;
    DWORD dwActiveProtocol, dwReaderLen, dwState, dwProt, dwAtrLen;
    BYTE pbAtr[300];
    char pbReader[300];
    int reader_nb;
    int i, offset;

    unsigned char bRecvBuffer[300];
    DWORD length;
    unsigned char attribute[1];
    DWORD attribute_length;
    SCARD_IO_REQUEST pioRecvPci;

    BYTE bSendBuffer[10];


    // �������� ������� ������
    memcpy(bSendBuffer, "\xE0\x00\x00\x58\x00", 5);
    DWORD controlcode = SCARD_CTL_CODE(3500);
    length = sizeof(bRecvBuffer);
    int rv = SCardControl(m_handle_card, controlcode, bSendBuffer, 5,
                      bRecvBuffer, sizeof(bRecvBuffer), &length);
    if (rv == SCARD_S_SUCCESS) {
        QString hextodec(toHexString(bRecvBuffer, length));
      //  qDebug() << "recive data command" << hextodec;
        parseAPDUResponse(hextodec);
    } else {
    //    qDebug() << "fail recive data";
    }

    if (m_respon_apdu_code == "6300") {
        QTimer::singleShot(REQUEST_CHECK_TIME_MSEC, this, SLOT(waitRequestData()));
    } else {
        qDebug() << "READ DATA:" << m_respon_apdu_code;
    }
}

QString PCSC::toHexString(LPBYTE baData, DWORD dataLen)
{
    QByteArray ba((char*)baData, (int)dataLen);
    return (QString(ba.toHex()));
}

void PCSC::parseAPDUResponse(QString responseText)
{
    bool ok;
    APDUResponse response;
    QTextStream text_stream(&responseText);
    text_stream.seek(0);
    response.CLA = text_stream.read(2);
    text_stream.seek(2);
    response.INS = text_stream.read(2);
    text_stream.seek(4);
    response.P1 = text_stream.read(2);
    text_stream.seek(6);
    response.P2 = text_stream.read(2);
    text_stream.seek(8);
    response.DataSize = text_stream.read(2);
    text_stream.seek(10);
    response.Data = text_stream.read(response.DataSize.toInt(&ok, 16)*2);

    //qDebug() << response.CLA << response.INS << response.P1 << response.P2 << response.DataSize << response.Data;
    //qDebug() << QByteArray::fromHex(response.Data.toAscii()).data();
    m_respon_apdu_code = response.Data.toAscii();
}

void PCSC::readCard()
{
    connect();
    waitForCardPresent();
    if (m_card_pressent) {

        activateCard();

        BYTE atr[40];
        INT	 atrLength;
        getAtrString(atr, &atrLength);

        // ��������� �������������� �����
        BYTE baCmdApduGetData[] = {0xFF, 0xCA, 0x00, 0x00, 0x00};
        BYTE baResponseApdu[300];
        DWORD lResponseApduLen = 0;
        exchange(baCmdApduGetData,
                 (DWORD)sizeof(baCmdApduGetData),
                 baResponseApdu,
                 &lResponseApduLen);

        //qDebug() << "size1" << lResponseApduLen;

        if( baResponseApdu[lResponseApduLen - 2] == 0x90 &&
            baResponseApdu[lResponseApduLen - 1] == 0x00)
        {
            QString hextodec("0x" + toHexString(baResponseApdu, lResponseApduLen - 2));
            bool ok;
            uint cardId16 = hextodec.toUInt(&ok, 16);
            m_card_uid.setNum(cardId16);

            QString hextodec2("0x" + toHexString(atr, sizeof(atr)));
            qDebug() << "hex to dec 1" <<hextodec2 ;

//            return;

            if (getClessCardType(atr) == PCSC::Mifare1K) {
                m_card_type = "MIFARE Classic 1k";
            } else if(getClessCardType(atr) == PCSC::Mifare4K) {
                m_card_type = "MIFARE Classic 4k";
            } else if(getClessCardType(atr) == PCSC::MifareUL) {
                m_card_type = "MIFARE Ultralight";
            } else if(getClessCardType(atr) == PCSC::MifareID) {
                m_card_type = "MIFARE ID";
            }
            emit cardInfo(m_card_type, m_card_uid);

        }

        // ��������� Payment System Environment
        BYTE baCmdApduGetData2[] = {0x00, 0xA4, 0x04, 0x00, 0x0E,  0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31};
        lResponseApduLen = 0;
        exchange(baCmdApduGetData2,
                 (DWORD)sizeof(baCmdApduGetData2),
                 baResponseApdu,
                 &lResponseApduLen);


        if( baResponseApdu[lResponseApduLen - 2] == 0x90 &&
            baResponseApdu[lResponseApduLen - 1] == 0x00)
        {
            qDebug() << "PSE" << "OK";
            QString hextodec2(toHexString(baResponseApdu, lResponseApduLen-2));
            QString code_type = hextodec2.mid(0, 2);
            QString length =  hextodec2.mid(2, 2);
            bool ok;
            QString message = hextodec2.mid(4, length.toUInt(&ok, 16));
            QString tag_84 = hextodec2.mid(4, 2);
            QString tag_84_length = hextodec2.mid(6, 2);
            QString tag_84_data = hextodec2.mid(8, (tag_84_length.toUInt(&ok, 16)*2));
            QString tag_a5 = hextodec2.mid(8 + (tag_84_length.toUInt(&ok, 16)*2), 2);
            QString tag_a5_length = hextodec2.mid(8 + (tag_84_length.toUInt(&ok, 16)*2) + 2, 2);
            QString tag_a5_data = hextodec2.mid(8 + (tag_84_length.toUInt(&ok, 16)*2) + 2 + 2, tag_a5_length.toUInt(&ok, 16));

            qDebug() << "RS:" << hextodec2;
            qDebug() << "READ DATA: = = = " << code_type << length.toUInt(&ok, 16) << message <<
                        tag_84 << tag_84_length << tag_84_length.toUInt(&ok, 16) << tag_84_data << tag_a5 <<
                        tag_a5_length << tag_a5_data;
        } else {
            //qDebug() << "PSE LOSE" << baResponseApdu[lResponseApduLen - 2] << baResponseApdu[lResponseApduLen - 1];
            QString hextodec2(toHexString(baResponseApdu, sizeof(baResponseApdu)));
        }
        m_card_pressent = false;

    }
    disconnect();

    QTimer::singleShot(CARD_CHECK_TIME_MSEC, this, SLOT(readCard()));
}

INT PCSC::cmpByte(LPBYTE array1, LPBYTE array2, INT len)
{
    INT i;
    for(i = 0; i < len; i++) {
        if (*array1++ != *array2++) {
            return i ? i: -1;
        }
    }

    return 0;
}

void PCSC::copyByte(LPBYTE des, LPBYTE src, INT len)
{
    INT i;
    for(i = 0; i < len; i++) {
        *des++ = *src++;
    }
}

LONG PCSC::getAtrString(LPBYTE atr, LPINT atrLen)
{
    LPBYTE   pbAttr = NULL;
    DWORD    cByte = SCARD_AUTOALLOCATE;
    LONG	 lRetValue;

    lRetValue = SCardGetAttrib(
                    m_handle_card,
                    SCARD_ATTR_ATR_STRING,
                    (LPBYTE)&pbAttr,
                    &cByte);
    PCSC_ERROR(lRetValue,"SCardGetAttrib (ATR_STRING)");

    if(atr != NULL) {
        copyByte(atr, pbAttr, cByte);
        *atrLen = cByte;
    }
    lRetValue = SCardFreeMemory(m_handle_context, pbAttr );
    PCSC_ERROR(lRetValue, "SCardFreeMemory");

    return lRetValue;
}

LONG PCSC::exchange(LPCBYTE pbSendBuffer, DWORD  cbSendLength,
                    LPBYTE  pbRecvBuffer, LPDWORD pcbRecvLength )
{
    LPCSCARD_IO_REQUEST  ioRequest;
    LONG lRetValue;

    switch (m_active_protocol)
    {
        case SCARD_PROTOCOL_T0:
            ioRequest = SCARD_PCI_T0;
            break;
        case SCARD_PROTOCOL_T1:
            ioRequest = SCARD_PCI_T1;
            break;
        default:
            ioRequest = SCARD_PCI_RAW;
            break;
    }

    *pcbRecvLength = RECEIVE_MAX_LENGTH;

    // APDU query
    lRetValue = SCardTransmit(m_handle_card,
                            ioRequest,
                            pbSendBuffer,
                            cbSendLength,
                            NULL,
                            pbRecvBuffer,
                            pcbRecvLength);
    PCSC_STATUS(lRetValue,"SCardTransmit");

    return lRetValue;
}

PCSC::CardType PCSC::getClessCardType(LPBYTE atr)
{
    if(cmpByte((LPBYTE)baAtrMifare1K, atr, sizeof(baAtrMifare1K)) == 0) {
        return Mifare1K;
    } else if(cmpByte((LPBYTE)baAtrMifare4K, atr, sizeof(baAtrMifare4K)) == 0) {
        return Mifare4K;
    } else if(cmpByte((LPBYTE)baAtrMifareUL, atr, sizeof(baAtrMifareUL)) == 0) {
        return MifareUL;
    } else if(cmpByte((LPBYTE)baAtrMifareID, atr, sizeof(baAtrMifareID)) == 0) {
        return MifareID;
    } else {
        return NotSupported;
    }
}
