#ifndef PCSC_H
#define PCSC_H

#include <QThread>
#include <QStringList>

#ifdef Q_OS_WIN
#include <winscard.h>
#endif
#ifdef Q_OS_LINUX
#include <PCSC/winscard.h>
#include <reader.h>

typedef int INT;
typedef char CHAR;
typedef int *LPINT;
#endif

#define RECEIVE_MAX_LENGTH 300l // Maximum APDU Buffer length.
#define CARD_WAIT_TIME_MSEC 30
#define CARD_CHECK_TIME_MSEC 30

#define REQUEST_CHECK_TIME_MSEC 1000

/**
 * @brief Class to work with smart cards
 *
 * Class runs in a separate thread.
 * The following features:
 * @li Connecting to the reader
 * @li Protocol definition interaction with smart card
 * @li Get Settings smart card
 * @li Getting the ID card
 */
class PCSC : public QThread
{
    Q_OBJECT
public:

    /**
     * @brief CardType
     */
    enum CardType
    {
        NotSupported, /**< The card format is not supported */
        Mifare1K, /**< Mifare1K */
        Mifare4K, /**< Mifare4K */
        MifareUL, /**< MifareUL */
        MifareID, /**< MifareID */
    };

    /**
     * @brief Default constructor
     * @param parent Object reference parent
     */
    explicit PCSC(QObject *parent = 0);
    /// The default destructor
    ~PCSC();

    /// Function run thread
    void run();

    /// The function return smart card type
    CardType getClessCardType(LPBYTE atr);
    /// The function init handle
    LONG connect();
    /// The function wait press card
    LONG waitForCardPresent();
    /// The function init smart card connection
    LONG activateCard();
    /// The function receives data type smart card
    LONG getAtrString(LPBYTE atr, LPINT atrLen);
    /// The function execute the APDU query
    LONG exchange(LPCBYTE pbSendBuffer ,DWORD  cbSendLength ,LPBYTE  pbRecvBuffer ,LPDWORD pcbRecvLength );
    /// The function disconnect from card and reader
    LONG disconnect();
    /// The function receives readers list
    QStringList getReaders();
    /// ����� �������� �����
    void emulationCard();
    void enterCommandState();
    /// ����� �������� �����
    void stopEmulationCard();
    static QString getScardErrorMessage(int code);

    /**
     * @brief The function copy byte
     * @param des Destantion
     * @param src Source
     * @param len Length
     */
    void copyByte(LPBYTE des, LPBYTE src, INT len);

    /**
     * @brief The function byte comparison
     * @param array1 First array
     * @param array2 Second array
     * @param len Array length
     * @return 0 is arrays are identical or number of bytes which eats the difference
     */
    INT cmpByte(LPBYTE array1,LPBYTE array2,INT len);
    /**
     * @brief The function convert byte to hex
     * @param baData Byte array
     * @param dataLen Array length
     * @return String format HEX
     */
    QString toHexString(LPBYTE baData, DWORD dataLen);

private:
    struct APDUResponse {
        QString CLA;
        QString INS;
        QString P1;
        QString P2;
        QString LE;
        QString DataSize;
        QString Data;
    };

    /// Resource manager handle
    SCARDCONTEXT m_handle_context;
    /// Card Handle
    SCARDHANDLE	m_handle_card;
    /// Selected card reader name
    CHAR m_size_selected_reader[256];
    /// Active protocol (T=0, T=1 or undefined)
    DWORD m_active_protocol;
    /// Active card reader
    QString m_reader;
    /// Lasr press card type
    QString m_card_type;
    /// Last press card UID
    QString m_card_uid;
    /// Flag, TRUE is card press, FALSE is card not press
    bool m_card_pressent;
    void parseAPDUResponse(QString responseText);

    QString m_respon_apdu_code;

signals:
    /**
     * @brief Signal available smart card
     * @param type Card type
     * @param uid Card UID
     */
    void cardInfo(const QString &type, const QString &uid);

private slots:
    /// The function read smart card
    void readCard();
    void waitRequestData();
};

#endif // PCSC_H
