#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QSettings>
#include <QFile>

SettingsDialog::SettingsDialog(QStringList readers, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_ShowModal, true);

    if (!readers.isEmpty()) {
        ui->f_readers->addItems(readers);
    }

    QFile file("settings.ini");
    if (file.exists()) {

        QSettings settings("settings.ini", QSettings::IniFormat);
        int index_current_reader = ui->f_readers->findText(
                    settings.value("reader_name").toString());
        if (index_current_reader != -1) {
            ui->f_readers->setCurrentIndex(index_current_reader);
        }
    }
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_a_cancel_clicked()
{
    close();
}

void SettingsDialog::on_a_Ok_clicked()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.setValue("reader_name", ui->f_readers->currentText());
    close();
}
