#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QWidget>
#include <QStringList>

namespace Ui {
class SettingsDialog;
}
/**
 * @brief Class customizes smart card reader to work
 *
 * Data is stored in the file settings.ini
 * @li reader_name Smart card reader name;
 */
class SettingsDialog : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Default constructor
     * @param readers Smard card readers list
     * @param parent Object reference parent
     */
    explicit SettingsDialog(QStringList readers, QWidget *parent = 0);
    /// The default destructor
    ~SettingsDialog();

private slots:
    /// The function cancel save settings
    void on_a_cancel_clicked();
    /// The function confirm save settings
    void on_a_Ok_clicked();

private:
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
