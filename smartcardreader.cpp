#include "smartcardreader.h"
#include "ui_smartcardreader.h"

#include <QFile>
#include <QMenu>

#include "settingsdialog.h"

SmartCardReader::SmartCardReader(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SmartCardReader)
{
    ui->setupUi(this);

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(showContextMenu(const QPoint &)));

    pcsc = new PCSC;
    connect(pcsc, SIGNAL(cardInfo(QString,QString)),
            this, SLOT(cardInfo(QString,QString)));

    QFile check_settings("./settings.ini");
    if (!check_settings.exists()) {
        showSettingsDialog();
    }

    pcsc->start();
}

SmartCardReader::~SmartCardReader()
{
    delete ui;
}

void SmartCardReader::cardInfo(const QString &type, const QString &uid)
{
    ui->f_cardType->setText(type);
    ui->f_cardUid->setText(uid);
}

void SmartCardReader::showContextMenu(const QPoint &pos)
{
    QMenu contextMenu(tr("Context menu"), this);

    QAction actionSettings(QString::fromUtf8("Settings"), this);
    connect(&actionSettings, SIGNAL(triggered()),
            this, SLOT(showSettingsDialog()));
    contextMenu.addAction(&actionSettings);

    contextMenu.exec(mapToGlobal(pos));
}

void SmartCardReader::showSettingsDialog()
{
    SettingsDialog *settings_dialog = new SettingsDialog(pcsc->getReaders());
    connect(settings_dialog, SIGNAL(destroyed(QObject*)),
            settings_dialog, SLOT(deleteLater()));
    settings_dialog->show();
}

void SmartCardReader::on_a_emulationCard_clicked()
{
    pcsc->emulationCard();
}

void SmartCardReader::on_a_stopEmulationCard_clicked()
{
    pcsc->stopEmulationCard();
}
