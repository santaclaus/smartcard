#ifndef SMARTCARDREADER_H
#define SMARTCARDREADER_H

#include <QWidget>

#include "pcsc.h"

namespace Ui {
class SmartCardReader;
}

class SmartCardReader : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Default constructor
     * @param parent Object reference parent
     */
    explicit SmartCardReader(QWidget *parent = 0);
    /// The default destructor
    ~SmartCardReader();

private:
    Ui::SmartCardReader *ui;
    /// Class to work with smart cards
    PCSC *pcsc;

private slots:
    /**
     * @brief The function receives data about the current card is presented
     * @param type Card type
     * @param uid Card UID
     */
    void cardInfo(const QString &type, const QString &uid);
    /**
     * @brief The function of a context menu
     * @param pos Position menu appears
     */
    void showContextMenu(const QPoint &pos);
    /// The function displays the settings dialog
    void showSettingsDialog();
    void on_a_emulationCard_clicked();
    void on_a_stopEmulationCard_clicked();
};

#endif // SMARTCARDREADER_H
